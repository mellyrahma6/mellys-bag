import 'package:mellys_bag/appBar.dart';
import 'package:mellys_bag/colorPick.dart';
import 'package:flutter/material.dart';
import 'package:mellys_bag/viewHome.dart';

class BerandaPage extends StatefulWidget {
  BerandaPage({Key key}) : super(key: key);

  @override
  _BerandaPageState createState() => _BerandaPageState();
}

class _BerandaPageState extends State<BerandaPage> {
  @override
  Widget build(BuildContext context) {
    return new SafeArea(
      child: Scaffold(
        appBar: AllAppBar(),
        body: _grid(),
        backgroundColor: Colors.amber[50],
      ),
    );
  }

  Widget _grid() {
    return new Padding(
      padding: const EdgeInsets.only(top: 20.0),
      child: GridView.count(
        childAspectRatio: 8.0 / 9.0,
        shrinkWrap: true,
        mainAxisSpacing: 20,
        crossAxisCount: 2,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.orangeAccent[100],
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/01/1.-Tas-Kerja-Fossil-Wanita-300x300.jpg",
                      cacheHeight: 115,
                      cacheWidth: 115,
                      fit: BoxFit.cover,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Fossil",
                      style: TextStyle(fontSize: 15),
                    ),
                    Text(
                      "Rp. 900.499",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.orangeAccent[100],
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/01/2.-Tas-Kerja-Gucci-Wanita-300x300.jpg",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Gucci",
                      style: TextStyle(fontSize: 15),
                    ),
                    Text(
                      "Rp. 20.000.000",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.orangeAccent[100],
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/01/3.-Tas-Kerja-Longchamp-Wanita-300x300.jpg",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Long Champ",
                      style: TextStyle(fontSize: 15),
                    ),
                    Text(
                      "Rp. 7000.000",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.orangeAccent[100],
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/01/6.-Tas-Kerja-Prada-Wanita-300x300.jpg",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Prada",
                      style: TextStyle(fontSize: 15),
                    ),
                    Text(
                      "Rp. 2.750.000",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.orangeAccent[100],
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/01/8.-Tas-Kerja-Sophie-Paris-Wanita-300x300.jpg",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Shopie",
                      style: TextStyle(fontSize: 15),
                    ),
                    Text(
                      "Rp. 390.000",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(0),
            child: Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(20.0),
                  ),
                  color: Colors.transparent),
              child: Center(
                  child: Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(10),
                    ),
                    color: Colors.orangeAccent[100],
                    border: Border.all(
                      color: Colors.black12,
                      width: 1,
                    )),
                padding: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Image.network(
                      "https://ecs7.tokopedia.net/blog-tokopedia-com/uploads/2019/01/7.-Tas-Kerja-Balenciaga-Wanita-300x300.jpg",
                      cacheHeight: 115,
                      cacheWidth: 115,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      "Balenciaga",
                      style: TextStyle(fontSize: 15),
                    ),
                    Text(
                      "Rp. 800.000",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              )),
            ),
          ),
        ],
      ),
    );
  }
}
