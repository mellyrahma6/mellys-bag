import 'package:mellys_bag/about.dart';
import 'package:mellys_bag/colorPick.dart';
import 'package:mellys_bag/home.dart';
import 'package:mellys_bag/models/contact.dart';
import 'package:mellys_bag/produk.dart';
import 'package:flutter/material.dart';
import 'package:mellys_bag/ui/entryform.dart';

class BelajarNavBar extends StatefulWidget {
  @override
  _BelajarNavBarState createState() => _BelajarNavBarState();
}

class _BelajarNavBarState extends State<BelajarNavBar> {
  static String tag = 'home-page';

  int _bottomNavCurrentIndex = 0;
  List<Widget> _container = [new BerandaPage(), EntryForm(), About()];

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: _container[_bottomNavCurrentIndex],
        bottomNavigationBar: _buildBottomNavigation());
  }

  Widget _buildBottomNavigation() {
    return new BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      onTap: (index) {
        setState(() {
          _bottomNavCurrentIndex = index;
        });
      },
      currentIndex: _bottomNavCurrentIndex,
      items: [
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.home,
            color: Colors.orangeAccent[100],
          ),
          icon: new Icon(
            Icons.home,
            color: Colors.grey,
          ),
          title: new Text(
            'Home',
          ),
        ),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.add_box_outlined,
            color: Colors.orangeAccent[100],
          ),
          icon: new Icon(
            Icons.add_box_outlined,
            color: Colors.grey,
          ),
          title: new Text('Tambah Bag'),
        ),
        BottomNavigationBarItem(
          activeIcon: new Icon(
            Icons.person,
            color: Colors.orangeAccent[100],
          ),
          icon: new Icon(
            Icons.person,
            color: Colors.grey,
          ),
          title: new Text('About'),
        ),
      ],
    );
  }
}
